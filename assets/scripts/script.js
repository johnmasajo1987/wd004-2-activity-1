// Functions - subprogram designed to do a particular task

// We need a function that will add two numbers.

// Syntax:
function nameoffunction(){
	// task
}

// remember, like your variables, function names should be descriptive.
// function names/variable names should be in camel case.
// parameters - values that the function needs to perform its task

// Function Declaration
function getSum(num1, num2){
	const sum = num1 + num2;
	console.log(sum);
	return(sum);
}

// function call
// Arguments - values that are sent to the function.
getSum(2, 3);
getSum(4, 5);
getSum(9, 8);

const ageSum = getSum(15,18);
console.log(ageSum);