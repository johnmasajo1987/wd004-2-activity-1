// Objects
// Data Types:
// 1. String
// 2. Number
// 3. Boolean
// 4. undefined - functions w/o a return value, variables that are declared but w/o assigned values
// 5. null - no value (you can explicity set this, or if the operation cannot find a value)
// 6. Objects - collection of related values; denoted by {}. Presented in key-value pairs.

// KEY - property name
// VALUE - value

// Objects Example

const student = {
	name: "Brandon", 
	studentNumber: "2014-15233",
	course: "BS Physics",
	department: "Science",
	age: 22,
	isSingle: true,
	motto: "Time is gold",
	address: {
		stName: "Mahogany St.",
		brgy: "Poblacion",
		city: "Makati City",
		zipCode: "12345"
	},
	showAge: function (){
		console.log(student.age);
		return student.age;
	},
	addAge: function(){
		student.age  += 1;
		return "Successfully Added Age"
	}
};

// ANONYMOUS FUNCTION - a function inside an objection. Also called "METHOD".

// To access a specific key/property of an object, we will use the dot notation.

// if you use dot notation after an object that does not exist, it will cause an error.

// if we access a property that does not exist, it will return UNDEFINED.

// to add a property in an object, we will use dot notation and assign the value.

student.gender = "Male";

// to update the value of a property, we will use dot notation and assign a new value.

student.isSingle = "false";

// to delete a property, we will use the keyword "delete" then object. propertyname

delete student.studentNumber;

// to change a value of a property in a property.

student.address.city = "Malabon";